# Converting a Mediawiki instance to Hugo

Mediawiki is a well known wiki platform, providing online editing capabilities.
The downside to this is that it is a dynamic website, requiring the resources
associated with a dynamic website; the need to maintain a database of users;
continually update the wiki engine for at least security vulnerabilities and
monitor for the unfortunately likely successful attempts to hack user accounts
(and handle the inevitable undesired content that results).

Hugo is a static site generator, content written in a markup language (which in
the case of Hugo is Markdown) is parsed by the generator during development
where it is combined with a theme (which may contain "dynamic" JavaScript
elements run client side) to produce a website that can be served to users
directly be a web server. The resources and technology required to serve these
pages are much lower; there is no need for a database of users to allow access
to the site and the site update process can be handled with the same workflow
as is used for other parts of the Apertis project (via gitlab, with the
associated review process).

## Process

1.  Retrieve a dump of the database associated with the Mediawiki instance that
is to be converted. This will require access to the server running the
Mediawiki instance. The following command can be used to perform the dump:

```
php dumpBackup.php --current --include-files --uploads > dump.xml
```

This will dump the current version of all pages (including pictures and alike)
from the database into the `dump.xml` file.

1.  Create a new repository in a suitable location in gitlab, based on the
`Pages/Hugo` template.

1.  Clone the repository locally.

1.  The Mediawiki dump can be converted into the required Markdown markup using
the scripts in this repository. This will delete existing example content from
the Hugo repo passed as an argument to the scrips and add in the content from
the wiki. The pages will be under `<hugo_repo>/content/`, the images and other
files under `<hugo_repo>/static/images`. The scripts can be run with the
following command:

```
./convert.sh <path_to_mediawiki_dump> <path_to_hugo_repo>
```

Note: If there are collisions in filenames, the scripts will add an `.alt`
extension. This will be recorded in the output from the script (along with any
other issues encountered) and these will need to be handled manually.

Note: We discovered a unique instance of ASCII art on a page that was causing
the conversion to fail. As it wasn't prevalent, the ASCII art was removed to
allow the conversion to take place.

1.  Build the website from the Markdown. The `README.md` created by the
`Pages/Hugo` template explains how to build with Hugo.

1.  Where HTML and Mediawiki markup have been used in a page, it is likely that
the resultant file will be converted to HTML rather than Markdown. This was
definitely the case for the main page of the Apertis website and this was
manually rewritten as a result.

# Post conversion clean-up

Whilst the data at this point is functionally converted to Markdown (with some
pages having large quantities of HTML elements due to that being used in some
of the wiki pages) and we were able to generate HTML pages using this, there is
quite a bit of clean-up that can be done to remove "wikiisms" from the pages
and structure of the site. These steps are not fully automated as many changes
require understanding of the content or require that the conversion is checked
to ensure the changes to the data are valid.

## Structural considerations

Hugo does not like to have pages and directories sharing the same name, for
example `foo.md` and `foo/`. Hugo will bias towards providing the contents of
the directory over the equivalently named Markdown file. With some restrictions
depending on the directory structure (and considering that pages in the
directory are probably related to the identically named Markdown file) the
Markdown file can be moved into the directory and renamed to `_index.md`. This
will make the file an [index page](https://gohugo.io/content-management/organization/#index-pages-_indexmd) and the page will be rendered with links to
the sub-pages at the bottom of it.

## Helper scripts

A number of scripts that have been written to aid with tidying up the Markdown. These are documented below:

### `check_links.py`

This script will parse through the
[`content` directory](https://gohugo.io/content-management/organization/)
looking at the URLs provided both my Markdown and HTML style links. It checks
that the provided URL matches one of the following patterns:

 - URLs beginning with a `#` are considered as links to fragments inside the
   existing document.
 - URLs starting with `http://` or `https://` are considered to be external
   links.
 - Links of the style `{{< ref "foo.md" >}}` are links to other Markdown
   documents.
 - Links starting with `/images/` are to images or other
   [static files](https://gohugo.io/content-management/static-files/).
 - Links which begin with `xml/` are special cased as this is used in some of
   the examples on the Apertis site.

The intention is to ensure that all links to other Markdown documents are of
the form `{{< ref "foo.md" >}}`. When links are provided in this form, Hugo
will refuse to build the site should the relevant Markdown page not be found.
This ensures that the links are not broken by changes to the site, ensuring at
least internal links remain valid.

### `move_page.py`

This script aids with moving pages. It will perform the page move (via `git mv`)
and then scan through the content directory looking for URLs which need to be
fixed up. This script does not add the changes to the git index to enable them
to be manually checked for correctness before being committed.

### `remove_dead_links.py`

This script looks for known dead links that were primarily added in automated
test reports previously added to the wiki and corrects them/removes them as
appropriate.

### `move_release_data.py`

This script moves the test reports from under `content/<version>/testreports/`
to `content/qa/weekly_tests/<version>/` and moves release information under
`content/release/<version>/`. The current layout means that there are a lot of
directories under the content root. This moves the test data to under QA where
it more logically lives and the release information under `release/`. This will
hopefully allow us to do some more fancy handling of this data at a later date
as part of custom Hugo templating.

### `resolve_redirects.py`

This script is run as part of the main conversion process and is documented here
for completeness.

Mediawiki supports redirection of pages by replacing the contents of the former
location with `# REDIRECT [<new location>]`, these links have been slightly
modified by the conversion and now read `1.  REDIRECT [<new location>]`. This
script finds instances of redirection, deleting the pages and replacing them
with an
[`alias`](https://gohugo.io/content-management/urls/#how-hugo-aliases-work) in
the front matter of the new location, which functionally has the same effect as
Mediawiki redirects in Hugo.

### `find_unlinked.py`

This script can be run to find pages that aren't explicit linked to by another
page. Please note that this script is not able to recognise when pages are
accessible via a list page and thus pages being listed here should not be taken
to mean that they are not accessible via the main page, but pages on this list
should be checked.

Pages can either be moved under an existing section with a list page, a new
subsection created or the pages linked to via a suitable location. It may also
be considered acceptable for certain pages to not be directly accessible via
the home page (for example, they might be accessible via header or footer links
configured in the site configuration, which isn't parsed by this script).

### `munge_test_cases.py`

The test cases are deprecated on the wiki and have been moved to qa.apertis.org.
A number of the test cases are currently redirected server side, others aren't
and reach a templated page on the wiki stating that the test case is obsolete.
A number of the test cases handled by the server side redirects have since been
obsoleted and now end up at a "404" page on qa.apertis.org. This script does a
number of things:

1. Looks for pages that contain the test case template showing they have been
   made obsolete. These pages are replaced with a simple page stating that the
   test case is obsolete.
2. An attempt is made to ascertain whether the remaining test cases are still
   valid by seeing if a test case with a matching name can be found on
   qa.apertis.org. In instances where the test case can be found, a custom page
   template is used to setup an HTML based page redirect.  For pages where no
   matching test case can be found, the page is replaced with a simple page as
   for (1).

As the pages contain aliases of the original wiki URLs, this approach means
that the server side redirects should be able to be dropped for the
qa.apertis.org redirection, making it easier for Apertis to maintain and
simplifying server side configuration.

### `resolve_vardefines.py`

This script is run as part of the main conversion process and is documented here
for completeness.

A number of wikimedia specific variables are defined in the documents. This
script parses the documents to find instances of defined variables. Instances
where these variables are used are then replaced with the relevant data and the
definitions removed.
