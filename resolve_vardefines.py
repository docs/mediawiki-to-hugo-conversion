#! /usr/bin/env python

import sys
import os
import mmap
import re

# We need to find and remove instances of `{{#vardefine:<key>|<value>}}`, then
# replace instances of `{{#var:<key>}}` with `<value>`.

if (len(sys.argv) < 2):
   print("Insufficient Arguments, provide xml file");
   sys.exit(1);

#print(sys.argv[1])

defines= {}

with open(sys.argv[1], 'r+') as fd:
    s = fd.readlines()
    for line in s:

        for (key, value) in re.findall("{{#vardefine:(?P<key>[A-Z_]+?)\|(?P<value>.+?)}}", line):
            if defines.has_key(key):
                if defines[key] != value:
                    print("Redefining %s from \"%s\" to \"%s\"" %(key, defines[key], value))
            defines[key] = value

        for key in defines.keys():
            #sys.stderr.write(re.escape("{{#vardefine:%s|%s}}" %(key, defines[key])))
            line = re.sub(re.escape("{{#vardefine:%s|%s}}" %(key, defines[key])), "", line)
            line = re.sub(re.escape("{{#var:%s}}" % (key)), "%s" %(defines[key]), line)

        print(line),
