#!/usr/bin/env python
import os
import re
import sys

# Move a page and update any references.

if len(sys.argv) < 3:
    print "%s <old_name> <new_name>" % sys.argv[0]
    sys.exit(1)

oldname = sys.argv[1]
newname = sys.argv[2]

if not os.getcwd().rsplit('/', 1)[1] == "content":
    print "ERROR: Script needs to run from content directory."
    print "CWD: ", os.getcwd().rsplit('/', 1)[1]
    sys.exit(1)

if not os.path.isfile(oldname):
    print "ERROR: \"%s\" is not a file." % oldname
    sys.exit(1)

if os.path.exists(newname):
    print "ERROR: \"%s\" already exists." % newname
    sys.exit(1)

os.system("git mv %s %s" %(oldname, newname))

md = []

# Get a list of all pages
for root, dirs, files in os.walk("."):
    for name in files:
        if name.endswith(".md"):
            md.append(os.path.join(root,name))

for page in md: 
    with open(page, 'r+') as fd:
        data = fd.read()

        data = re.sub(re.escape("/%s" % oldname), "/%s" % newname, data, re.MULTILINE)

        fd.seek(0)
        fd.write(data)
        fd.truncate()

