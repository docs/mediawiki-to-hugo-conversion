#!/usr/bin/env python
import os
import re
import sys

# Ensure that all internal links use `{{< ref "foo.md" >}}` as this helps
# protect against hanging links.

def checkurl(url):
    if (len(url.strip()) < 1):
        return 0

    if (url[0] == "#"):
        return 0

    if re.match("^http[s]?://", url):
        return 0

    if re.match("^[ ]*{{< ref \".*\" >}}[ ]*", url):
        return 0

    if re.match("^/images/", url):
        return 0

    # Needed for xml examples
    if re.match("^xml/", url):
        return 0

    return 1;

if len(sys.argv) < 2:
    print "%s <content> " % sys.argv[0]
    sys.exit(1)

os.chdir(sys.argv[1])

if not os.getcwd().rsplit('/', 1)[1] == "content":
    print "ERROR: Script needs to run from content directory."
    print "CWD: ", os.getcwd().rsplit('/', 1)[1]
    sys.exit(1)

md = []

# Get a list of all pages
for root, dirs, files in os.walk("."):
    for name in files:
        if name.endswith(".md"):
            md.append(os.path.join(root,name))

retval = 0

for page in md: 
    with open(page, 'r+') as fd:
        data = fd.read()

        for link in re.finditer('\]\((?P<url>.*?)\)', data, re.MULTILINE | re.DOTALL):
            if checkurl(link.group('url')):
                print "ERROR: URL in %s doesn't conform to required patterns: \"%s\"" % (page, link.group('url'))
                retval = 1

        for link in re.finditer('href=\"(?P<url>.*?)\"', data, re.MULTILINE | re.DOTALL):
            if checkurl(link.group('url')):
                print "ERROR: URL in %s doesn't conform to required patterns: \"%s\"" % (page, link.group('url'))
                retval = 1

sys.exit(retval)
