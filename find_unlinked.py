#!/usr/bin/env python
import os
import re
import sys

path = sys.argv[1]

md = []
links = {}

def traverse (start="_index.md"):
    page = links.pop(start, None)

    if page != None:
        for item in page:
            traverse(item)
            #print "%s:%s" %(start, page)

# Get a list of all pages
os.chdir(sys.argv[1])
for root, dirs, files in os.walk("."):
    for name in files:
        if name.endswith(".md"):
            md.append(os.path.join(root,name))

#print "digraph G {"
for name in md:
    page = name.lstrip("./")
    links[page] = []
    with open(page, 'r+') as fd:
        data = fd.read()
        for m in re.finditer('{{< ref "(?P<url>.*)" >}}', data):
            #print "\"%s\" -> \"%s\"" % (page, m.group('url').lstrip("./"))
            links[page].append(m.group('url').lstrip("./"))
#print "}"

# Let's traverse the list and remove all pages linked to _index.md
traverse()

for item in sorted(links.keys()):
    print item
