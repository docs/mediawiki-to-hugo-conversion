#!/bin/bash

if [ ! -f "$1" ]
then
    echo "Can't find Mediawiki dump file"
    exit 1
fi

if [ ! -f "$2/config.toml" ]
then
    echo "Can't find Hugo directory"
    exit 1
fi

SITE="$2"

rm -Rvf export/*
rm -Rvf ${SITE}/content
rm -Rvf ${SITE}/static/images

echo "Resolving vardefines"
./resolve_vardefines.py $1 > test.dump

cd mediawiki-to-markdown

echo "Check (and install) required tools"
if [ ! -d "vendor" ]
then
    curl -sS https://getcomposer.org/installer | php
    php composer.phar install
fi

echo "Converting to markdown"
php convert.php --filename=../test.dump --output=../export

cd ..

# Check all static files are used
TEMPFILE=$(mktemp)
find export/static -type f > $TEMPFILE

while IFS= read -r FILE
do
    grep -Riq "${FILE##*/}" export/content/*
    if [ "$?" != "0" ]; then
        echo "Removing ${FILE} as not used"
        rm ${FILE}
    fi
done < $TEMPFILE

mv export/content ${SITE}
mv export/static/images ${SITE}/static

# Wikis allow hanging links to non-existant pages, Hugo doesn't. There are a
# number of pages that we know are missing. Fill these in with a placeholder so
# that we can build the page.

function create_missing {
    RELPATH="${SITE}/content$1"
    if [ ! -d $(dirname "$RELPATH") ]
    then
        mkdir -p $(dirname $RELPATH)
    fi

    cat <<EOF >> $RELPATH
+++
title = "Missing File"
+++
This file did not exist in the wiki import.
EOF

    echo "Created missing file: $RELPATH"
}

create_missing "/_category_qa/test_cases.md"
create_missing "/_category_qa/test_report.md"
create_missing "/_template_bugcomment.md"
create_missing "/_template_bug.md"
create_missing "/bbz__429.md"
create_missing "/bbz_75.md"
create_missing "/category_archive.md"
create_missing "/category_arm_image.md"
create_missing "/category_obsolete.md"
create_missing "/category_pages_that_need_updating.md"
create_missing "/category_pages_that_need_updating_on_{{{todo}}}.md"
create_missing "/category_qa/obsolete_test_cases.md"
create_missing "/category_qa/deprecated_test_suites.md"
create_missing "/category_qa/integrated_test_suites.md"
create_missing "/category_qa/ostree_test_report.md"
create_missing "/category_qa/test_suites.md"
create_missing "/category_qa/test_suite/template.md"
create_missing "/category_qa/tiny-containers_test_report.md"
create_missing "/category_qa/under_development_test_suites.md"
create_missing "/qa/test_cases/gstreamer-video-playback.md"
create_missing "/qa/test_cases/gstreamer-video-record.md"
create_missing "/qa/test_cases/r2.6.1/clutter-camera-player.md"
create_missing "/special_newpages.md"
create_missing "/special_recentchanges.md"
create_missing "/specs.md"
create_missing "/v2019.2/release_schedule.md"
create_missing "/v2020.0/release_schedule.md"
create_missing "/v2020pre/testreports/release.md"
create_missing "/v2021dev1/release_schedule.md"
create_missing "/wikipedia_creative_commons.md"
create_missing "/wikipedia_free_and_open-source_software.md"
create_missing "/wikipedia_free_software.md"
create_missing "/wikipedia_peer_review.md"
create_missing "/wikipedia_open-source_software.md"

echo "Resolving redirects"
./resolve_redirects.py ${SITE}/content
