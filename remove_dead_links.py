#!/usr/bin/env python
import os
import re
import sys
import urllib

# Remove dead links to pages we no longer want links to.

links = [
    { 'old':'[Category:QA/Test_report/{{\#rel2abs:\n../../.}}]()', 'new': ''},
    { 'old':'[Category:QA/Test\nreport/{{\#rel2abs:\n../../../.}}]()', 'new': ''},
    { 'old':'[Category:QA/Test report/{{\#rel2abs:\n../../../.}}]()', 'new': ''},
    { 'old':'[Category:QA/OSTree test\nreport]( {{< ref "/category_qa/ostree_test_report.md" >}} )', 'new': ''},
    { 'old':'[Category:QA/tiny-containers test\nreport]( {{< ref "/category_qa/tiny-containers_test_report.md" >}} )', 'new': ''},
    { 'old':'"https://www.apertis.org/category_qa/test_report/"', 'new':'"https://www.apertis.org/qa/weekly_tests/"'},
    { 'old':'Category:QA/Test report', 'new':'Test Reports'},
    { 'old':'Category:QA/Test_report', 'new':'Test Reports'},
    { 'old':'"/category_qa/test_report.md"', 'new':'"/qa/weekly_tests"'},
    { 'old':'"/_category_qa/test_cases.md"', 'new':'"/qa/test_cases"'}
]

if len(sys.argv) < 2:
    print "Need to provide path to content"
    sys.exit(1)

path = sys.argv[1]

os.chdir(path)

md = []

# Get a list of all pages
for root, dirs, files in os.walk("."):
    for name in files:
        if name.endswith(".md"):
            md.append(os.path.join(root,name))

for page in md: 
    with open(page, 'r+') as fd:
        data = fd.read()

        for link in links:
            data = re.sub(re.escape(link['old']), link['new'], data, re.MULTILINE)

        fd.seek(0)
        fd.write(data)
        fd.truncate()

