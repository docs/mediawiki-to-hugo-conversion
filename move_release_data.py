#! /usr/bin/env python

import mmap
import os
import re
import sys

# Move test reports from under `content/<version>/testreports/` to
# `content/qa/weekly_tests/<version>/` and move release information
# under `content/release/<version>/`.

if (len(sys.argv) < 2):
   print("Insufficient Arguments, provide markdown directory");
   sys.exit(1);

dirs = []

# Get a list of relevant dirs
os.chdir(sys.argv[1])
for item in os.listdir("."):
    if os.path.isdir(item):
        # Find directories that match one of the versioning schemes
        if re.match("v[0-9]{4}", item) or re.match("[0-9]{2}\.[0-9]{2}", item):
            dirs.append(item)

#print "dir:", dirs

moved = []
indexes = []
releases = []

for version in dirs:
    if os.path.exists("%s/testreports" % version):
        oldname = "%s/testreports" % version
        newname = "qa/weekly_tests/%s" % version
        os.system("git mv %s %s" % (oldname, newname))
        print "git mv %s %s" % (oldname, newname)
        moved.append({'old': oldname, 'new': newname})
    if os.path.exists("%s/testreports.md" % version):
        oldname = "%s/testreports.md" % version
        os.system("git rm %s" % oldname)
        print "git rm %s" % oldname
        indexes.append(oldname)
    if os.path.exists(version):
        oldname = "%s" % version
        newname = "release/%s" % version
        os.system("git mv %s %s" % (oldname, newname))
        print "git mv %s %s" % (oldname, newname)
        releases.append({'old': oldname, 'new': newname})

md = []

# Get a list of all pages
for root, dirs, files in os.walk("."):
    for name in files:
        if name.endswith(".md"):
            md.append(os.path.join(root,name))

# Replace any links in the pages to avoid redirects
for page in md:
    with open(page, 'r+') as fd:
        data = fd.read()
    
        for names in moved:
            data = re.sub(re.escape("{{< ref \"/%s\" >}}" % names['old']), "{{< ref \"/%s\" >}}" % names['new'], data)
            before = r'\{\{\<\ ref\ \"\/%s(\/.*)\"\ \>\}\}' % re.escape(names['old'])
            #print "before: ", before
            after = r'{{< ref "/%s\1" >}}' % names['new']
            #print "after: ", after
            data = re.sub(before, after, data)

        for name in indexes:
            data = re.sub(re.escape("{{< ref \"/%s\" >}}" % name), "{{< ref \"/qa/weekly_tests/_index.md\" >}}", data)

        for names in releases:
            data = re.sub(re.escape("{{< ref \"/%s\" >}}" % names['old']), "{{< ref \"/%s\" >}}" % names['new'], data)
            before = r'\{\{\<\ ref\ \"\/%s(\/.*)\"\ \>\}\}' % re.escape(names['old'])
            #print "before: ", before
            after = r'{{< ref "/%s\1" >}}' % names['new']
            #print "after: ", after
            data = re.sub(before, after, data)

        fd.seek(0)
        fd.write(data)
        fd.truncate()

