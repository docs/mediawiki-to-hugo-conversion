#! /usr/bin/env python

import mmap
import os
import re
import sys

# Replace redirects with aliases in the required location

if (len(sys.argv) < 2):
   print("Insufficient Arguments, provide markdown directory");
   sys.exit(1);

md = []

# Get a list of all pages
os.chdir(sys.argv[1])
for root, dirs, files in os.walk("."):
    for name in files:
        if name.endswith(".md"):
            md.append(os.path.join(root,name))

new_aliases = {}
delete_me = []

#print md

# Read all pages and build an array of aliases
for page in md:
    with open(page, 'r+') as fd:
        data = fd.read()
        match = re.search('^1.  REDIRECT[\s]+\[.*\]\( {{< ref "(?P<url>.*)" >}} \)', data, re.MULTILINE | re.DOTALL)
        if (match):
            if not match.group('url') in new_aliases:
                new_aliases[match.group('url')] = []
            new_aliases[match.group('url')].append(page.lstrip("."))

            # Also grab aliases as we don't want to loose them
            aliases = re.search('aliases = \[[\s]+"(?P<alias>.*)"[\s]+\]', data, re.MULTILINE | re.DOTALL)
            if(aliases):
                new_aliases[match.group('url')].append(aliases.group('alias'))

            # List of pages to delete
            delete_me.append(page)

#print new_aliases

# Add new aliases to files
for target in new_aliases.keys():
    # Read existing file and extract existing aliases
    #print "target: %s" % target
    try:
        with open(".%s" % target, 'r+') as fd:
            data = fd.read()
            # Also grab aliases as we don't want to loose them
            aliases = re.search('aliases = \[[\s]+"(?P<alias>.*)"[\s]+\]', data, re.MULTILINE | re.DOTALL)
            if(aliases):
                new_aliases[target].append(aliases.group('alias'))
    
                # Build complete new alias string
                alias_string = "\",\n    \"".join(new_aliases[target])
                #print "Alias string for %s: %s" %(target, alias_string)
            
                # Replace alias string in file data
                data = re.sub(aliases.group('alias'), alias_string, data)
            
                # Write back file
                fd.seek(0)
                fd.write(data)
                fd.truncate()
            else:
                print "No aliases in .%s???" % target
                print "Dropping REDIRECTS:"
                print new_aliases[target]
    except:
        print "Failed to handle %s" % target

# Replace any links in the pages to avoid redirects
for page in md:
    with open(page, 'r+') as fd:
        data = fd.read()
        for target in new_aliases.keys():
            for alias in new_aliases[target]:
                data = re.sub(re.escape("{{< ref \"%s\" >}}" % alias), "{{< ref \"%s\" >}}" % target, data)
                before = r'\{\{\<\ ref\ \"%s(#.*)\"\ \>\}\}' % re.escape(alias)
                after = r'{{< ref "%s\1" >}}' % target
                data = re.sub(before, after, data)
        fd.seek(0)
        fd.write(data)
        fd.truncate()

# Delete redirect pages
for page in delete_me:
    #print "Delete: %s" % page
    os.remove(page)
