#!/usr/bin/env python
import os
import re
import sys
import urllib

if len(sys.argv) < 2:
    print "Need to provide path to test cases"
    sys.exit(1)

path = sys.argv[1]

os.chdir(path)

md = []

# Get a list of all pages
for root, dirs, files in os.walk("."):
    for name in files:
        if name.endswith(".md"):
            md.append(os.path.join(root,name))

# Replace any links in the pages to avoid redirects 
for page in md: 
    with open(page, 'r+') as fd:
        data = fd.read()

        fm = re.search('\+\+\+(?P<fm>.*)\+\+\+', data, re.MULTILINE | re.DOTALL)
        if not fm:
            continue

        print "Filename: %s\n" % page
        name = re.sub('^./', '', page)
        name = re.sub('.md$', '', name)
        #print "Name: %s\n" % name

        #print "Front matter:"
        #print fm.group('fm')

        if not re.search('{{ TestCase', data):
            continue

        dep = re.search('{{ TestCase.*state = deprecated', data, re.MULTILINE | re.DOTALL)

	link = urllib.urlopen("https://qa.apertis.org/%s.html" % name)
        
        if dep or link.getcode() == 404:
            matter = re.sub("title.*", "title = \"%s\"" % name, fm.group('fm'))
            data = "+++%s+++\n" % matter
            data += "This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/\n"
        else:
            matter = re.sub("title.*", "title = \"%s\"\ntype = \"redirect\"" % name, fm.group('fm'))
            data = "+++%s+++\n" % matter

        #print data
        fd.seek(0)
        fd.write(data)
        fd.truncate()

