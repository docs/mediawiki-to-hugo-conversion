
<?php

$arguments = arguments($argv);

require 'vendor/autoload.php';

function write_file($path, $text) {
    // May contain path elements that need to be created
    $parts = explode('/', $path);
    $filename = array_pop($parts);
    $directory = implode('/', $parts);

    // create directory if necessary
    if(!empty($directory)) {
	if(!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }
    }

    // Check for collisions
    if(file_exists($path)) {
        $path_new = $path . ".alt";
        echo "File collision on $path, renaming to $path_new\n";
        $path = $path_new;
    }

    // Create file
    $file = fopen($path, 'w');
    fwrite($file, $text);
    fclose($file);
}

// Load arguments passed from CLI 

if(empty($arguments['filename'])) {
    echo "No input file specified. Use --filename=mediawiki.xml" . PHP_EOL . PHP_EOL; 
    exit;
}

if(!empty($arguments['output'])) {
    $output_path = $arguments['output'];
        
    if(!file_exists($output_path)) {
        echo "Creating output directory $output_path" . PHP_EOL . PHP_EOL;
        mkdir($output_path);
    }

} else {
    $output_path = '';
}

if (substr($output_path, -1) != '/') $output_path = $output_path . '/';

if(!empty($arguments['format'])) {
    $format = $arguments['format'];
} else {
    $format = 'gfm';
}


if(!empty($arguments['fm']) OR (empty($arguments['fm']) && $format == 'gfm')) {
    $add_meta = true;
} else {
    $add_meta = false;
}




// Load XML file
$file = file_get_contents($arguments['filename']);

$xml = str_replace('xmlns=', 'ns=', $file); //$string is a string that contains xml... 

$xml = new SimpleXMLElement($xml);


$result = $xml->xpath('page');
$count = 0;
$directory_list = array();

// Iterate through XML
while(list( , $node) = each($result)) {
    
    $title = $node->xpath('title');
    $title = $title[0];
    // Sanitise title
    $url = str_replace(' ', '_', $title);
    $original_title = $url;
    $url = str_replace(':', '_', $url);
    $url = strtolower($url);
    $url = preg_replace('/^file_/', '', $url);

    $date = $node->xpath('revision/timestamp');
    $date = explode("T", $date[0])[0];

    if ($slash = strpos($url, '/')){
        $title = str_replace('/', ' ', $title);
        $directory = substr($url, 0, $slash);
        $filename = substr($url, $slash+1);
        $directory_list[$directory] = true;
    } else {
        $directory = '';
        $filename = $url;
    }

    // Treat "main_page" differently so that we end up with an index page in
    // the root folder
    if ($filename == 'main_page' && $directory == '') {
        $filename = '_index';
    }

    // Let's see if we have an uploaded component
    $text = $node->xpath('upload/contents');
    if ($text) {
        $path = normalizePath($output_path . "static/images/" . $directory . '/' . $filename);
        $text = base64_decode($text[0]);
        write_file($path, $text);
        $count++;
	continue;
    }

    $text = $node->xpath('revision/text');
    $text = $text[count($text) - 1];
    $text = html_entity_decode($text); // decode inline html
    $text = preg_replace_callback('/\[\[(.+?)\]\]/', "new_link", $text); // adds leading slash to links, "absolute-path reference"

    // Prepare to append page title frontmatter to text
    if ($add_meta) {    
        $frontmatter = "+++\n";
/*
        $frontmatter .= "fragment = \"content\"\n";
*/
	$frontmatter .= "date = \"$date\"\n";
	$frontmatter .= "weight = 100\n";
	$frontmatter .= "\n";
        $frontmatter .= "title = \"$title\"\n";
        $frontmatter .= "\n";
        $frontmatter .= "aliases = [\n";
        $frontmatter .= "    \"/old-wiki/$original_title\"\n";
        $frontmatter .= "]\n";
        $frontmatter .= "+++\n\n";
    }

    $pandoc = new Pandoc\Pandoc();
    $options = array(
        "from"  => "mediawiki",
        "to"    => $format
    );
    $text = $pandoc->runWith($text, $options);


    // Post processing of pandoc output to improve fidelity
    $text = str_replace('\_', '_', $text);
    $text = str_replace('__NOTOC__', '', $text);
    $text = str_replace('__NOEDITSECTION__', '', $text);

    // Improve URL handling
    $text = preg_replace_callback('/\[([^]]*)\]\(([^)]*)\)/s', "format_wiki_link", $text);

    // Some embedded HTML that we need to sanitise too.
    $text = preg_replace_callback('/<a href="([^"]+)" title="wikilink">([^<]*)<\/a>/s', "format_html_wikilink", $text);
    $text = preg_replace_callback('/<a href="([^"]+)">([^<]*)<\/a>/s', "format_html_link", $text);

    if ($add_meta) {
        $text = $frontmatter . $text;
    }

    $path = normalizePath($output_path . "content/" . $directory . '/' . $filename . '.md');

    write_file($path, $text);
/*
    // Structure for Syna
    $path = normalizePath($output_path . "content/" . $directory . '/' . $filename . '/content.md');

    write_file($path, $text);

    $text = "+++\n";
    $text .= "title = \"$title\"\n";
    $text .= "date = \"$date\"\n";
    $text .= "+++\n";

    $path = normalizePath($output_path . "content/" . $directory . '/' . $filename . '/index.md');

    write_file($path, $text);
*/

    $count++;
}


// Rename and move files with the same name as directories
if (!empty($directory_list) && !empty($arguments['indexes'])) {

    $directory_list = array_keys($directory_list);

    foreach ($directory_list as $directory_name) {

        if(file_exists($output_path . $directory_name . '.md')) {
            rename($output_path . $directory_name . '.md', $output_path . $directory_name . '/index.md');
        }
    }

}

if ($count > 0) {
    echo "$count files converted" . PHP_EOL . PHP_EOL;
}


function arguments($argv) {
    $_ARG = array();
    foreach ($argv as $arg) {
      if (preg_match('/--([^=]+)=(.*)/',$arg,$reg)) {
        $_ARG[$reg[1]] = $reg[2];
      } elseif(preg_match('/-([a-zA-Z0-9])/',$arg,$reg)) {
            $_ARG[$reg[1]] = 'true';
        }
  
    }
  return $_ARG;
}

function format_html_wikilink($part) {
    $text = $part[2];
    $path = $part[1];

    $data = format_link($part[1], true);

    if ($data['is_image']) {
        return "<img src=\"{$data['url']}\" alt=\"{$part[2]}\">";
    } else {
        return "<a href=\"{$data['url']}\">{$part[2]}</a>";
    }
}

function format_html_link($part) {
    $text = $part[2];
    $path = $part[1];

    $data = format_link($part[1], false);

    if ($data['is_image']) {
        return "<img src=\"{$data['url']}\" alt=\"{$part[2]}\">";
    } else {
        return "<a href=\"{$data['url']}\">{$part[2]}</a>";
    }
}

function format_wiki_link($part) {
    $prelink = "";
    $is_wikilink = false;

    // Detect wikilinks
    if (strpos($part[2], " \"wikilink\"") != false) {
        $is_wikilink = true;
        $part[2] = str_replace(' "wikilink"', "", $part[2]);
    }

    $data = format_link($part[2], $is_wikilink);

    if ($data['is_image']) {
        $prelink = "!";
    }
    return "{$prelink}[{$part[1]}]({$data['url']})";
}

function format_link($part, $is_wikilink) {
    $imagetypes = array('gif', "jpeg", "jpg", "png", "svg");
    $prelink = false;
    $is_static = false;

    // Nobble MediaWiki ParserFunctions
    if (strpos($part, "{{#") != false) {
        // Kill URL, it probably won't make any sense without the ParserFunction anyway
        $part = "";
        $is_wikilink = false;
    }

    // Split down URL further, first find any in-documented component
    $component = parse_url($part);

    // Some URLs may not have a path or not be parseable
    if ((is_bool($component) == false) && in_array("path", array_keys($component))) {
        $path = pathinfo($component['path']);

        // Detect images
        if (in_array("extension", array_keys($path))) {
            $is_static = true;
            if (in_array($path['extension'], $imagetypes)) {
                $prelink = true;
            }
        }
    } else {
        // If we have no path, treat as remote link
        $is_wikilink = false;
    }

    // Can now format remote stuff
    if ($is_wikilink == false) {
        $retval['is_image'] = $prelink;
        $retval['url'] = $part;

        return $retval;
    }

    // Remove "File:" from file links
    $component['path'] = str_replace('File:', "", $component['path']);

    // Replace colons with underscores
    $component['path'] = str_replace(':', '_', $component['path']);

    // In-page links should contain dashes not underscores and be lower case
    if (in_array("fragment", array_keys($component))) {
        $component['fragment'] = strtolower(str_replace('_', '-', $component['fragment']));

        // Handle just in-page links
        if ($path['filename'] == "") {
            $retval['is_image'] = $prelink;
            $retval['url'] = "#{$component['fragment']}";
            return $retval;
        }
    }

    $tmp = strtolower($component['path']);
    if ($prelink) {
        $tmp = "/images$tmp";
    }

    if (in_array("fragment", array_keys($component))) {
        $fragment = "#{$component['fragment']}";
    } else {
        $fragment = "";
    }

    if ($is_static) {
        $preurl = "";
        $extension = "";
        $posturl = "";
    } else {
        $preurl = " {{< ref \"";
        $extension = ".md";
        $posturl = "\" >}} ";
    }


    $retval['is_image'] = $prelink;
    $retval['url'] = "$preurl$tmp$extension$fragment$posturl";
    return $retval;
}

function new_link($matches){
    if(strpos($matches[1], '|') != true) {
        $new_link = str_replace(' ', '_', $matches[1]);
        return "[[/$new_link|${matches[1]}]]";
    } else {

        $link = trim(substr($matches[1], 0, strpos($matches[1], '|')));
        $link = '/' . str_replace(' ', '_', $link);

        $link_text = trim(substr($matches[1], strpos($matches[1], '|')+1));

        return "[[$link|$link_text]]";
    }
}


// Borrowed from http://php.net/manual/en/function.realpath.php
function normalizePath($path)
{
    $parts = array();                         // Array to build a new path from the good parts
    $path = str_replace('\\', '/', $path);    // Replace backslashes with forwardslashes
    $path = preg_replace('/\/+/', '/', $path);// Combine multiple slashes into a single slash
    $segments = explode('/', $path);          // Collect path segments
    $test = '';                               // Initialize testing variable
    foreach($segments as $segment)
    {
        if($segment != '.')
        {
            $test = array_pop($parts);
            if(is_null($test))
                $parts[] = $segment;
            else if($segment == '..')
            {
                if($test == '..')
                    $parts[] = $test;
                if($test == '..' || $test == '')
                    $parts[] = $segment;
            }
            else
            {
                $parts[] = $test;
                $parts[] = $segment;
            }
        }
    }
    return implode('/', $parts);
}


?>
